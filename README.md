# Game Galaxy REST API

## Confuguration

For using the application you must create a file named ```ormconfig.json``` with the following contents:

```
    {
    "type": "postgres",
    "host": hostname,
    "port": port,
    "username": username,
    "password": passowrd,
    "database": databaseName,
    "synchronize": false,
    "logging": false,
    "entities": [
       "build/models/*.{ts,js}"
    ],
    "migrations": [
       "build/migrations/*.{js,ts}"
    ],
    "cli": {
        "migrationsDir": "src/migrations"
    },
    "ssl": true,
    "extra": {
        "ssl": {
            "rejectUnauthorized": false
        }
    }
 }
```

Also, a file named ```keys.json``` needs to be created with the following contents:

```
    {
        "gapi": "Google APIs API key with access to YouTube data"
    }
```

Lastly, a file named ```firebase-key.json``` is needed. This file can be obtained from the Firebase Console Settings, selecting the Service Accounts option.

NOTE: A postgres instance, a firebase account and a Youtube API Key are needed.

## Installing dependencies

For installing dependencies, execute the following command (NOTE: this step is needed before testing, building or running the server):

```
    yarn install
```

## Testing

For running testing with coverage included, execute the following command:

```
    yarn test
```

## Building

For building the project, execute the following command (NOTE: this step is needed before running the server):

```
    yarn build
```

## Running

For running the server, execute the following command:

```
    yarn start
```