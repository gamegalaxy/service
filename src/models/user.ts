import { Entity, Column, PrimaryColumn, OneToMany, ManyToOne } from 'typeorm';
import UserRole from '../enums/userRole';
import Component from './component';
import LibraryGame from './library-game';

@Entity()
class User {

    @PrimaryColumn()
    id: string;

    @Column()
    name: string;

    @Column()
    email: string;

    @OneToMany(() => LibraryGame, libraryGame => libraryGame.user)
    games: LibraryGame[];

    @ManyToOne(() => Component, component => component.userCPUS)
    CPU: Component;

    @ManyToOne(() => Component, component => component.userGPUS)
    GPU: Component;

    @Column({
        type:"enum", 
        enum: UserRole, 
        array: true, 
        default: [UserRole.User]
    })
    roles: UserRole[];

}

export default User;