import { Entity, Column, PrimaryGeneratedColumn, ManyToMany } from 'typeorm';
import Game from './game';
import LibraryGame from './library-game';

@Entity()
class Platform {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @Column()
    isPC: boolean;

    @Column()
    icon: string;

    @ManyToMany(() => Game, game => game.platforms)
    games: Game[];

    @ManyToMany(() => LibraryGame, libraryGame => libraryGame.platforms)
    ownedGames: Game[];

}

export default Platform;