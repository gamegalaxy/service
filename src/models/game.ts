import { Entity, Column, PrimaryGeneratedColumn, ManyToOne, ManyToMany, OneToMany, JoinTable } from 'typeorm';
import Component from './component';
import LibraryGame from './library-game';
import Platform from './platform';

@Entity()
class Game {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    title: string;

    @Column()
    cover: string;

    @Column()
    description: string;

    @Column()
    developer: string;

    @Column()
    genre: string;

    @Column()
    launchDate: Date;

    @ManyToMany(() => Platform, platform => platform.games)
    @JoinTable()
    platforms: Platform[];

    @OneToMany(() => LibraryGame, libraryGame => libraryGame.game)
    ownedGames: Platform[];

    @ManyToOne(() => Component, component => component.minG)
    minGPU: Component;

    @ManyToOne(() => Component, component => component.minC)
    minCPU: Component;

    @ManyToOne(() => Component, component => component.recG)
    recGPU: Component;

    @ManyToOne(() => Component, component => component.recC)
    recCPU: Component;
}

export default Game;