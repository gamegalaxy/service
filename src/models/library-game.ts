import { Entity, PrimaryGeneratedColumn, ManyToMany, ManyToOne, JoinTable } from 'typeorm';
import Game from './game';
import Platform from './platform';
import User from './user';

@Entity()
class LibraryGame {

    @PrimaryGeneratedColumn()
    id: number;

    @ManyToOne(() => Game)
    game: Game;

    @ManyToOne(() => User, user => user.games)
    user: User;

    @ManyToMany(() => Platform, platform => platform.ownedGames)
    @JoinTable()
    platforms: Platform[];

}

export default LibraryGame;