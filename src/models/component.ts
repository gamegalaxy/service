import { Entity, Column, PrimaryGeneratedColumn, OneToMany } from 'typeorm';
import ComponentBrand from '../enums/componentBrand';
import ComponentType from '../enums/componentType';
import Game from './game';
import User from './user';

@Entity()
class Component {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    type: ComponentType;

    @Column()
    brand: ComponentBrand;

    @Column()
    model: string;

    @Column()
    benchmark: number;

    @OneToMany(() => Game, game => game.minGPU)
    minG: Game[];

    @OneToMany(() => Game, game => game.minCPU)
    minC: Game[];
    
    @OneToMany(() => Game, game => game.recGPU)
    recG: Game[];

    @OneToMany(() => Game, game => game.recCPU)
    recC: Game[];

    @OneToMany(() => User, user => user.CPU)
    userCPUS: User[];

    @OneToMany(() => User, user => user.GPU)
    userGPUS: User[];

}

export default Component;