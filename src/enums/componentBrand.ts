enum ComponentBrand {
    Intel,
    AMD,
    Nvidia,
    Qualcomm
}

export default ComponentBrand;