import { NextFunction, Request, Response } from "express";
import { StatusCodes } from "http-status-codes";
import firebaseService from "../services/firebaseService";

var authentication = async (request: Request, response: Response, next: NextFunction) => {
    var token = request.headers.authorization;
    if(!token) {
        return response.status(StatusCodes.BAD_REQUEST).json({
            message: "Missing authentication token."
        });
    }
    if(token.split(' ').length > 1) {
        var idToken = token.split(' ')[1];
        try {
            var t = await firebaseService.verifyUser(idToken)
            request.headers.user_id = t.uid;
            request.headers.user_email = t.email;
            return next();
        } catch(e) {
            return response.status(StatusCodes.UNAUTHORIZED).json({
                message: "Invalid authentication token."
            });
        }
        
    } else {
        return response.status(StatusCodes.BAD_REQUEST).json({
            message: "Missing authentication token."
        });
    }
}

export default authentication;