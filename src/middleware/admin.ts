import { Logger } from "@overnightjs/logger";
import { NextFunction, Request, Response } from "express";
import { StatusCodes } from "http-status-codes";
import { db_connection } from "../data/connection";
import UserRole from "../enums/userRole";
import User from "../models/user";

var admin = (request: Request, response: Response, next: NextFunction) => {
    var users = db_connection.getRepository(User)
    var user_id = request.headers.user_id;
    if(!user_id) {
        response.status(StatusCodes.BAD_REQUEST).json({
            message: "Missing user id."
        });
        return;
    }
    
    users.findOne(user_id as string).then(user => {
        if(user.roles.includes(UserRole.Admin)) {
            return next();
        } else {
            response.status(StatusCodes.UNAUTHORIZED).json({
                message: "Unauthorized."
            });
            return;
        }
    })
}

export default admin;