import { after, describe, it } from "mocha";
import * as sn from "sinon";
import firebaseService from "../services/firebaseService";
import authentication from "./authentication";
import { NextFunction, request, Request, Response } from "express";

const mockResponse = () => {
    const res = {} as any;
    res.status = sn.stub().returns(res);
    res.json = sn.stub().returns(res);
    return res;
};

const mockRequest = (auth: string | undefined) => {
    const res = {
        headers: {}
    } as any;
    res.headers.authorization = auth;
    return res;
};

const mockNext = sn.stub();
var fbStub;

describe('authentication middleware', () => {

    before(() => {
        fbStub = sn.stub(firebaseService, "verifyUser");
    })
    
    after(() => {
        fbStub.restore();
    });

    it('should be bad request when empty token provided', async () => {
        var res = await authentication(mockRequest(undefined), mockResponse(), mockNext);
        sn.assert.calledOnceWithExactly((res as any).status, 400);
    });

    it('should be bad request when not bearer token provided', async () => {
        var res = await authentication(mockRequest("abc"), mockResponse(), mockNext);
        sn.assert.calledOnceWithExactly((res as any).status as any, 400);
    });
    
    it('should be unathorized when invalid token provided', async () => {
        fbStub.returns(new Promise((_, re) => re()));
        var res = await authentication(mockRequest("Bearer abc"), mockResponse(), mockNext);
        sn.assert.calledOnceWithExactly((res as any).status, 401);
    });

    it('should be ok when valid token provided', async () => {
        fbStub.returns(new Promise((rs, _) => rs({
            uid: "",
            email: "",
            photo: ""
        })));
        var res = await authentication(mockRequest("Bearer abc"), mockResponse(), mockNext);
        sn.assert.calledOnce(mockNext);
    });

});