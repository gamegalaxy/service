import * as bodyParser from 'body-parser';
import { Server as OServer } from '@overnightjs/core';
import { Logger } from '@overnightjs/logger';
import { ComponentsController, GameController, LibraryController, MediaController, PlatformsController, PlayableController, SearchController, UserController } from './controllers';
import connection from './data/connection';
import User from './models/user';
import Component from './models/component';
import Game from './models/game';
import Platform from './models/platform';
import LibraryGame from './models/library-game';
import cors from 'cors';
 
export class Server extends OServer {
    
    constructor() {
        super(process.env.NODE_ENV === 'development');
        this.app.use(bodyParser.json());
        this.app.use(bodyParser.urlencoded({extended: true}));

        var corsOptions = {
            origin: '*',
        }

        this.app.use(cors(corsOptions))
        this.setupControllers();
    }
 
    private setupControllers(): void {

        connection().then(c => {
            var users = c.getRepository(User);
            var components = c.getRepository(Component);
            var games = c.getRepository(Game);
            var platforms = c.getRepository(Platform);
            var library = c.getRepository(LibraryGame);

            const userController = new UserController(users, components);
            const componentsController = new ComponentsController(components);
            const gamesController = new GameController(games);
            const searchController = new SearchController(games);
            const mediaController = new MediaController(games);
            const platformsController = new PlatformsController(platforms);
            const libraryController = new LibraryController(library, users, games, platforms);
            const playableController = new PlayableController(games, users);
            super.addControllers(
                [
                    userController,
                    componentsController,
                    gamesController,
                    searchController,
                    mediaController,
                    platformsController,
                    libraryController,
                    playableController
                ],
            );
        });

    }
 
    public start(port: number): void {
        this.app.listen(port, () => {
            Logger.Imp('Server listening on port: ' + port);
        })
    }
}