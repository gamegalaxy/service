import { Server } from "./server";

const port = Number(process.env.PORT || 8080);

var server = new Server();
server.start(port);