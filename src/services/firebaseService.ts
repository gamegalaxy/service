import * as admin from 'firebase-admin';
import { Logger } from '@overnightjs/logger';

class FirebaseService {

    constructor() {
        var cert = require('../../firebase-key.json');
        admin.initializeApp({
            credential: admin.credential.cert(cert)
        });
    }

    verifyUser(token: string): Promise<admin.auth.DecodedIdToken> {
        return new Promise<admin.auth.DecodedIdToken>((resolve, reject) => {
            admin.auth().verifyIdToken(token)
                .then((decodedToken) => {
                    Logger.Info("Succesful authentication");
                    resolve(decodedToken);
                }).catch((error) => {
                    Logger.Err("Authentication failed");
                    Logger.Err(error);
                    reject();
                });
        })
        
    }

}

export default new FirebaseService();