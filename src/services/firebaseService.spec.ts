import { assert } from 'chai';
import * as admin from 'firebase-admin';
import * as sn from 'sinon';
import firebaseService from './firebaseService';

var fbStub: sn.SinonStub<any>;
var st;

describe('firebase service', () => {

    before(() => {
        st = sn.stub(admin, "initializeApp");
        fbStub = sn.stub(admin, "auth");
    })
    
    after(() => {
        fbStub.restore();
        st.restore();
    });

    it('should succeed with right token', async () => {
        fbStub.returns({
            verifyIdToken: () => new Promise((rs, re) => rs("abc"))
        });
        firebaseService.verifyUser("abc").then(t => {
            assert.equal(t as any, "abc");
        }).catch(e => {
            assert.equal(false, true);
        });
    });

    it('should fail with wrong token', async () => {
        console.log(admin.auth());
        fbStub.returns({
            verifyIdToken: () => new Promise((rs, re) => re("abc"))
        });
        firebaseService.verifyUser("abc").then(t => {
            assert.equal(false, true);
        }).catch(e => {

        });
    });

});