import {MigrationInterface, QueryRunner} from "typeorm";

export class FixedJoinAtGame1606697137013 implements MigrationInterface {
    name = 'FixedJoinAtGame1606697137013'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "game_platforms_platform" ("gameId" integer NOT NULL, "platformId" integer NOT NULL, CONSTRAINT "PK_c1ca4cb0aecf11874462fb18d93" PRIMARY KEY ("gameId", "platformId"))`);
        await queryRunner.query(`CREATE INDEX "IDX_552929816b601dba123ca67fac" ON "game_platforms_platform" ("gameId") `);
        await queryRunner.query(`CREATE INDEX "IDX_5cc01d015d6bea3c04251c67da" ON "game_platforms_platform" ("platformId") `);
        await queryRunner.query(`ALTER TABLE "game_platforms_platform" ADD CONSTRAINT "FK_552929816b601dba123ca67facf" FOREIGN KEY ("gameId") REFERENCES "game"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "game_platforms_platform" ADD CONSTRAINT "FK_5cc01d015d6bea3c04251c67daa" FOREIGN KEY ("platformId") REFERENCES "platform"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "game_platforms_platform" DROP CONSTRAINT "FK_5cc01d015d6bea3c04251c67daa"`);
        await queryRunner.query(`ALTER TABLE "game_platforms_platform" DROP CONSTRAINT "FK_552929816b601dba123ca67facf"`);
        await queryRunner.query(`DROP INDEX "IDX_5cc01d015d6bea3c04251c67da"`);
        await queryRunner.query(`DROP INDEX "IDX_552929816b601dba123ca67fac"`);
        await queryRunner.query(`DROP TABLE "game_platforms_platform"`);
    }

}
