import {MigrationInterface, QueryRunner} from "typeorm";

export class JoinHotfix1606647308712 implements MigrationInterface {
    name = 'JoinHotfix1606647308712'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "library_game_platforms_platform" ("libraryGameId" integer NOT NULL, "platformId" integer NOT NULL, CONSTRAINT "PK_62f66873bf6e40197aed4d83d11" PRIMARY KEY ("libraryGameId", "platformId"))`);
        await queryRunner.query(`CREATE INDEX "IDX_0f8793a735cc7d01125383a796" ON "library_game_platforms_platform" ("libraryGameId") `);
        await queryRunner.query(`CREATE INDEX "IDX_6f6c39cad9ebeab284d31f16cc" ON "library_game_platforms_platform" ("platformId") `);
        await queryRunner.query(`ALTER TABLE "library_game_platforms_platform" ADD CONSTRAINT "FK_0f8793a735cc7d01125383a7968" FOREIGN KEY ("libraryGameId") REFERENCES "library_game"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "library_game_platforms_platform" ADD CONSTRAINT "FK_6f6c39cad9ebeab284d31f16cc9" FOREIGN KEY ("platformId") REFERENCES "platform"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "library_game_platforms_platform" DROP CONSTRAINT "FK_6f6c39cad9ebeab284d31f16cc9"`);
        await queryRunner.query(`ALTER TABLE "library_game_platforms_platform" DROP CONSTRAINT "FK_0f8793a735cc7d01125383a7968"`);
        await queryRunner.query(`DROP INDEX "IDX_6f6c39cad9ebeab284d31f16cc"`);
        await queryRunner.query(`DROP INDEX "IDX_0f8793a735cc7d01125383a796"`);
        await queryRunner.query(`DROP TABLE "library_game_platforms_platform"`);
    }

}
