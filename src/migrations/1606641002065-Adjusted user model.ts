import {MigrationInterface, QueryRunner} from "typeorm";

export class AdjustedUserModel1606641002065 implements MigrationInterface {
    name = 'AdjustedUserModel1606641002065'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "user" DROP COLUMN "picture"`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "user" ADD "picture" character varying NOT NULL`);
    }

}
