import {MigrationInterface, QueryRunner} from "typeorm";

export class AddedRoles1606621287771 implements MigrationInterface {
    name = 'AddedRoles1606621287771'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TYPE "user_roles_enum" AS ENUM('0', '1')`);
        await queryRunner.query(`ALTER TABLE "user" ADD "roles" "user_roles_enum" array NOT NULL DEFAULT '{0}'`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "user" DROP COLUMN "roles"`);
        await queryRunner.query(`DROP TYPE "user_roles_enum"`);
    }

}
