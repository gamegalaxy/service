import {MigrationInterface, QueryRunner} from "typeorm";

export class InitialMigration1606614862297 implements MigrationInterface {
    name = 'InitialMigration1606614862297'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "platform" ("id" SERIAL NOT NULL, "name" character varying NOT NULL, "isPC" boolean NOT NULL, "icon" character varying NOT NULL, CONSTRAINT "PK_c33d6abeebd214bd2850bfd6b8e" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "user" ("id" character varying NOT NULL, "name" character varying NOT NULL, "email" character varying NOT NULL, "picture" character varying NOT NULL, "cPUId" integer, "gPUId" integer, CONSTRAINT "PK_cace4a159ff9f2512dd42373760" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "library_game" ("id" SERIAL NOT NULL, "gameId" integer, "userId" character varying, CONSTRAINT "PK_6a3d9faf8629dc53cb87c2b9e3b" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "game" ("id" SERIAL NOT NULL, "title" character varying NOT NULL, "cover" character varying NOT NULL, "description" character varying NOT NULL, "developer" character varying NOT NULL, "genre" character varying NOT NULL, "launchDate" TIMESTAMP NOT NULL, "minGPUId" integer, "minCPUId" integer, "recGPUId" integer, "recCPUId" integer, CONSTRAINT "PK_352a30652cd352f552fef73dec5" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "component" ("id" SERIAL NOT NULL, "type" integer NOT NULL, "brand" integer NOT NULL, "model" character varying NOT NULL, "benchmark" integer NOT NULL, CONSTRAINT "PK_c084eba2d3b157314de79135f09" PRIMARY KEY ("id"))`);
        await queryRunner.query(`ALTER TABLE "user" ADD CONSTRAINT "FK_3934de61bafb94b72d9086f4a68" FOREIGN KEY ("cPUId") REFERENCES "component"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "user" ADD CONSTRAINT "FK_9d93579db5bfa5dfcd9d83c3258" FOREIGN KEY ("gPUId") REFERENCES "component"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "library_game" ADD CONSTRAINT "FK_6b8becfe30c69e601acdd06968d" FOREIGN KEY ("gameId") REFERENCES "game"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "library_game" ADD CONSTRAINT "FK_d3c27759d65ed4c2b2da78877f5" FOREIGN KEY ("userId") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "game" ADD CONSTRAINT "FK_bfe3cdfcfe9cd757d834fc35a2e" FOREIGN KEY ("minGPUId") REFERENCES "component"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "game" ADD CONSTRAINT "FK_aed93106d9112d795e2d1c93025" FOREIGN KEY ("minCPUId") REFERENCES "component"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "game" ADD CONSTRAINT "FK_676c55e593984997ffe78ab731b" FOREIGN KEY ("recGPUId") REFERENCES "component"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "game" ADD CONSTRAINT "FK_a82d6d8798d84f775bbedfb73b8" FOREIGN KEY ("recCPUId") REFERENCES "component"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "game" DROP CONSTRAINT "FK_a82d6d8798d84f775bbedfb73b8"`);
        await queryRunner.query(`ALTER TABLE "game" DROP CONSTRAINT "FK_676c55e593984997ffe78ab731b"`);
        await queryRunner.query(`ALTER TABLE "game" DROP CONSTRAINT "FK_aed93106d9112d795e2d1c93025"`);
        await queryRunner.query(`ALTER TABLE "game" DROP CONSTRAINT "FK_bfe3cdfcfe9cd757d834fc35a2e"`);
        await queryRunner.query(`ALTER TABLE "library_game" DROP CONSTRAINT "FK_d3c27759d65ed4c2b2da78877f5"`);
        await queryRunner.query(`ALTER TABLE "library_game" DROP CONSTRAINT "FK_6b8becfe30c69e601acdd06968d"`);
        await queryRunner.query(`ALTER TABLE "user" DROP CONSTRAINT "FK_9d93579db5bfa5dfcd9d83c3258"`);
        await queryRunner.query(`ALTER TABLE "user" DROP CONSTRAINT "FK_3934de61bafb94b72d9086f4a68"`);
        await queryRunner.query(`DROP TABLE "component"`);
        await queryRunner.query(`DROP TABLE "game"`);
        await queryRunner.query(`DROP TABLE "library_game"`);
        await queryRunner.query(`DROP TABLE "user"`);
        await queryRunner.query(`DROP TABLE "platform"`);
    }

}
