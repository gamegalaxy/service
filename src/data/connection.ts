import { Logger } from "@overnightjs/logger";
import { exit } from "process";
import { Connection, createConnection } from "typeorm";

export var db_connection: Connection;

var connection = (): Promise<Connection> => {
    return new Promise<Connection>((resolve, _) => {
        if(db_connection) {
            resolve(db_connection);
        } else {
            createConnection().then(c => {
                db_connection = c;
                resolve(c);
            }).catch(error => {
                Logger.Err("Error connecting to db");
                Logger.Err(error);
                exit(-1);
            });
        }
    });
};

export default connection;