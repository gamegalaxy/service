import { UserController } from './user';
import { ComponentsController } from './components';
import { GameController } from './game';
import { SearchController } from './search';
import { MediaController } from './media';
import { PlatformsController } from './platforms';
import { LibraryController } from './library';
import { PlayableController } from './playable';

export {
    ComponentsController,
    UserController,
    GameController,
    SearchController,
    MediaController,
    PlatformsController,
    LibraryController,
    PlayableController
}