import { StatusCodes } from 'http-status-codes';
import { Controller, Delete, Get, Middleware, Post } from '@overnightjs/core';
import { Request, Response } from 'express';
import { Repository } from 'typeorm';
import { Logger } from '@overnightjs/logger';
import Game from '../models/game';
import authentication from '../middleware/authentication';
import admin from '../middleware/admin';
 
@Controller('game')
export class GameController {

    games: Repository<Game>;

    constructor(games: Repository<Game>) {
        this.games = games;
    }

    @Get()
    private getAll(request: Request, response: Response) {
        Logger.Info("Get games from ");
        this.games.find({ relations: ['platforms'] })
        .then(c => {
            if(c.length > 0) {
                return response.status(StatusCodes.OK).json(c);
            } else {
                return response.status(StatusCodes.NOT_FOUND).json("Not Found");
            }
        })
        .catch(e => {
            Logger.Err(e);
            return response.sendStatus(StatusCodes.NOT_FOUND);
        });
    }

    @Get(':id')
    private getOne(request: Request, response: Response) {
        Logger.Info("Get game " + request.params.id);
        this.games.findOne(request.params.id, { relations: [ 'minGPU', 'minCPU', 'recGPU', 'recCPU', 'platforms' ] })
        .then(c => {
            if(c) {
                return response.status(StatusCodes.OK).json(c);
            } else {
                return response.status(StatusCodes.NOT_FOUND).json("Not Found");
            }
        })
        .catch(e => {
            Logger.Err(e);
            return response.sendStatus(StatusCodes.NOT_FOUND);
        });
    }

    @Delete(':id')
    @Middleware([authentication, admin])
    private delete(request: Request, response: Response) {
        Logger.Info("Get game " + request.params.id);
        this.games.findOne(request.params.id)
        .then(c => {
            if(c) {
                this.games.delete(c);
                return response.sendStatus(StatusCodes.OK);
            } else {
                return response.sendStatus(StatusCodes.NOT_FOUND);
            }
        })
        .catch(e => {
            Logger.Err(e);
            return response.sendStatus(StatusCodes.NOT_FOUND);
        });
    }

    @Post()
    @Middleware([authentication, admin])
    private post(request: Request, response: Response) {
        Logger.Info("New game");
        var body = request.body;
        if(body.title && body.cover && body.description &&
            body.developer && body.genre && body.launchDate &&
            body.platforms) {
            
            var game = new Game();
            game.title = body.title;
            game.cover = body.cover;
            game.description = body.description;
            game.developer = body.developer;
            game.genre = body.genre;
            game.launchDate = body.launchDate;
            game.platforms = body.platforms;

            this.games.save(game);

            return response.status(StatusCodes.OK).json(game);
            
        }  else {
            return response.sendStatus(StatusCodes.BAD_REQUEST);
        }
    }

    @Post(':id')
    @Middleware([authentication, admin])
    private async put(request: Request, response: Response) {
        Logger.Info("Update game " + request.params.id);
        var body = request.body;
        var game = new Game();
        var id = request.params.id as any as number;
        try {
            var actualGame = await this.games.findOne(id, { relations: ['platforms'] });
            if(body.title) {
                game.title = body.title;
            }
            if(body.cover) {
                game.cover = body.cover;
            }
            if(body.description) {
                game.description = body.description;
            }
            if(body.developer) {
                game.developer = body.developer;
            }
            if(body.genre) {
                game.genre = body.genre;
            }
            if(body.launchDate) {
                game.launchDate = body.launchDate;
            }
            if(body.platforms) {
                this.games
                .createQueryBuilder()
                .relation(Game, 'platforms')
                .of(actualGame)
                .addAndRemove(body.platforms, actualGame.platforms);
            }
            this.games.update(id, game);    
            return response.sendStatus(StatusCodes.OK);
        } catch(e) {
            Logger.Err(e);
            return response.sendStatus(StatusCodes.INTERNAL_SERVER_ERROR);
        }
    }

}