import { StatusCodes } from 'http-status-codes';
import { ClassMiddleware, Controller, Get } from '@overnightjs/core';
import { Request, Response } from 'express';
import { Repository } from 'typeorm';
import { Logger } from '@overnightjs/logger';
import Game from '../models/game';
import authentication from '../middleware/authentication';
import User from '../models/user';
 
@Controller('playable')
@ClassMiddleware([authentication])
export class PlayableController {

    games: Repository<Game>;
    users: Repository<User>;

    constructor(games: Repository<Game>, users: Repository<User>) {
        this.games = games;
        this.users = users;
    }

    @Get(':id')
    private async verify(request: Request, response: Response) {
        Logger.Info("Get if playable of game " + request.params.id);
        try {
            var user = await this.users.findOne(request.headers.user_id as string, { relations: ['CPU', 'GPU'] });
            console.log(user);
            var game = await this.games.findOne(request.params.id, { relations: ['minCPU', 'minGPU'] });
            console.log(game);
            if(!game) {
                return response.sendStatus(StatusCodes.NOT_FOUND);
            }
            if(user.CPU != null && user.GPU != null && game.minCPU != null && game.minGPU != null &&
                user.CPU != undefined && user.GPU != undefined && game.minCPU != undefined && game.minGPU != undefined) {
                if(user.CPU.benchmark >= game.minCPU.benchmark
                    && user.GPU.benchmark >= game.minGPU.benchmark) {
                    return response.status(StatusCodes.OK).json({ playable: true });
                } else {
                    return response.status(StatusCodes.OK).json({ playable: false });
                }
            } else {
                return response.sendStatus(StatusCodes.NOT_FOUND);
            }
        } catch(e) {
            Logger.Err(e);
            return response.sendStatus(StatusCodes.INTERNAL_SERVER_ERROR);
        }
    }

}