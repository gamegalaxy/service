import { StatusCodes } from 'http-status-codes';
import { Controller, Get } from '@overnightjs/core';
import { Request, Response } from 'express';
import { Repository } from 'typeorm';
import { Logger } from '@overnightjs/logger';
import Game from '../models/game';
import Axios from 'axios';
 
@Controller('media')
export class MediaController {

    games: Repository<Game>;
    key: string;

    constructor(games: Repository<Game>) {
        this.games = games;
        this.key = require('../../keys.json').gapi;
    }

    @Get('game/:id')
    private async getOne(request: Request, response: Response) {
        Logger.Info("Get game " + request.params.id);
        try {
            var c = await this.games.findOne(request.params.id)
            console.log(c)
            if(c) {
                const url = `https://www.googleapis.com/youtube/v3/search?part=id,snippet&maxResults=5&type=video&videoEmbeddable=true&key=${ this.key }&q=${ c.title }`;
                console.log(url);
                var res = await Axios.get(url)
                console.log(res.data);
                return response.status(StatusCodes.OK).json(res.data);
            } else {
                return response.status(StatusCodes.NOT_FOUND).json("Not Found");
            }
        } catch(e) {
            Logger.Err(e);
            return response.sendStatus(StatusCodes.INTERNAL_SERVER_ERROR);
        }
    }

}