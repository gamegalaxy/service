import { StatusCodes } from 'http-status-codes';
import { Controller, Get } from '@overnightjs/core';
import { Request, Response } from 'express';
import { Repository } from 'typeorm';
import { Logger } from '@overnightjs/logger';
import Platform from '../models/platform';
 
@Controller('platforms')
export class PlatformsController {

    platforms: Repository<Platform>;

    constructor(platforms: Repository<Platform>) {
        this.platforms = platforms;
    }

    @Get()
    private getAll(request: Request, response: Response) {
        Logger.Info("Get platforms");
        this.platforms.find()
        .then(c => {
            if(c.length > 0) {
                return response.status(StatusCodes.OK).json(c);
            } else {
                return response.status(StatusCodes.NOT_FOUND).json("Not Found");
            }
        })
        .catch(e => {
            Logger.Err(e);
            return response.sendStatus(StatusCodes.NOT_FOUND);
        });
    }
}