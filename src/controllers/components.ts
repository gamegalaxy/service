import { StatusCodes } from 'http-status-codes';
import { Controller, Get } from '@overnightjs/core';
import { Request, Response } from 'express';
import { Repository } from 'typeorm';
import { Logger } from '@overnightjs/logger';
import Component from '../models/component';
import ComponentType from '../enums/componentType';
 
@Controller('components')
export class ComponentsController {

    components: Repository<Component>;

    constructor(components: Repository<Component>) {
        this.components = components;
    }

    @Get()
    private getAll(request: Request, response: Response) {
        Logger.Info("Get components");
        this.components.find()
        .then(c => {
            if(c.length > 0) {
                return response.status(StatusCodes.OK).json(c);
            } else {
                return response.status(StatusCodes.NOT_FOUND).json("Not Found");
            }
        })
        .catch(e => {
            Logger.Err(e);
            return response.sendStatus(StatusCodes.NOT_FOUND);
        });
    }

    @Get('/details/:id')
    private get(request: Request, response: Response) {
        Logger.Info("Get component " + request.params.id);
        var id = request.params.id as any as number;
        this.components.findOne(id)
        .then(c => {
            if(c) {
                return response.status(StatusCodes.OK).json(c);
            } else {
                return response.sendStatus(StatusCodes.NOT_FOUND);
            }
        })
        .catch(e => {
            Logger.Err(e);
            return response.sendStatus(StatusCodes.INTERNAL_SERVER_ERROR);
        });
    }

    @Get('cpu')
    private getCPU(request: Request, response: Response) {
        Logger.Info("Get CPU components");
        this.components.find({ type: ComponentType.CPU })
        .then(c => {
            if(c.length > 0) {
                return response.status(StatusCodes.OK).json(c);
            } else {
                return response.status(StatusCodes.NOT_FOUND).json("Not Found");
            }
        })
        .catch(e => {
            Logger.Err(e);
            return response.sendStatus(StatusCodes.NOT_FOUND);
        });
    }

    @Get('gpu')
    private getGPU(request: Request, response: Response) {
        Logger.Info("Get CPU components");
        this.components.find({ type: ComponentType.GPU })
        .then(c => {
            if(c.length > 0) {
                return response.status(StatusCodes.OK).json(c);
            } else {
                return response.status(StatusCodes.NOT_FOUND).json("Not Found");
            }
        })
        .catch(e => {
            Logger.Err(e);
            return response.sendStatus(StatusCodes.NOT_FOUND);
        });
    }

}