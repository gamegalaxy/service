import { StatusCodes } from 'http-status-codes';
import { ClassMiddleware, Controller, Delete, Get, Post } from '@overnightjs/core';
import { Request, Response } from 'express';
import { Repository } from 'typeorm';
import { Logger } from '@overnightjs/logger';
import authentication from '../middleware/authentication';
import LibraryGame from '../models/library-game';
import User from '../models/user';
import Game from '../models/game';
import Platform from '../models/platform';
 
@Controller('library')
@ClassMiddleware([authentication])
export class LibraryController {

    ownedGames: Repository<LibraryGame>;
    users: Repository<User>;
    games: Repository<Game>;
    platforms: Repository<Platform>;

    constructor(ownedGames: Repository<LibraryGame>, 
        users: Repository<User>, games: Repository<Game>,
        platforms: Repository<Platform>) {
        this.ownedGames = ownedGames;
        this.users = users;
        this.games = games;
        this.platforms = platforms;
    }

    @Get()
    private async get(request: Request, response: Response) {
        var user_id = request.headers.user_id as string;
        try {
            var lib = await this.ownedGames.find({ where: { user: { id: user_id } }, relations: ['platforms', 'game']});
            return response.status(StatusCodes.OK).json(lib);
        } catch(e) {
            return response.sendStatus(StatusCodes.INTERNAL_SERVER_ERROR);
        }
    }

    @Post()
    private async add(request: Request, response: Response) {
        Logger.Info("Add to Library");
        var body = request.body;
        var user_id = request.headers.user_id as string;
        if(!body.game && !body.platform) {
            return response.sendStatus(StatusCodes.BAD_REQUEST);
        }
        try {
            var lib = await this.ownedGames.find({ where: { user: { id: user_id }, game: { id: body.game } }, relations: ['platforms']});
            if(lib.length > 0) {
                var platform = await this.platforms.findOneOrFail(body.platform);
                if(!lib[0].platforms.includes(platform)) {
                    lib[0].platforms.push(platform);
                    this.ownedGames.save(lib[0]);
                }
                return response.status(StatusCodes.OK).json(lib[0]);
            } else {
                var own = new LibraryGame();
                own.game = await this.games.findOneOrFail(body.game);
                own.user = await this.users.findOneOrFail(user_id);
                own.platforms = [await this.platforms.findOneOrFail(body.platform)];
                this.ownedGames.save(own);
                return response.status(StatusCodes.OK).json(own);
                
            }
        } catch(e) {
            Logger.Err(e);
            return response.sendStatus(StatusCodes.INTERNAL_SERVER_ERROR);
        }
    }

    @Delete(':game/:platform')
    private async delete(request: Request, response: Response) {
        Logger.Info("Delete to Library");
        var params = request.params;
        var user_id = request.headers.user_id as string;
        if(!params.game && !params.platform) {
            return response.sendStatus(StatusCodes.BAD_REQUEST);
        }
        try {
            var lib = await this.ownedGames.find({ where: { user: { id: user_id }, game: { id: params.game } }, relations: ['platforms']});
            console.log(lib);
            if(lib.length > 0) {
                var index = lib[0].platforms.findIndex(x => String(x.id) == params.platform);
                if(index == -1) {
                    return response.sendStatus(StatusCodes.NOT_FOUND);
                }
                console.log(index);
                lib[0].platforms.splice(index, 1);
                this.ownedGames.save(lib[0]);
                console.log(lib[0])
                if(lib[0].platforms.length == 0) {
                    this.ownedGames.delete(lib[0].id);
                }
                return response.status(StatusCodes.OK).json(lib[0]);
            } else {
                var own = new LibraryGame();
                own.game = await this.games.findOneOrFail(params.game);
                own.user = await this.users.findOneOrFail(user_id);
                own.platforms = [await this.platforms.findOneOrFail(params.platform)];
                this.ownedGames.save(own);
                return response.status(StatusCodes.OK).json(own);
                
            }
        } catch(e) {
            Logger.Err(e);
            return response.sendStatus(StatusCodes.INTERNAL_SERVER_ERROR);
        }
    }

}