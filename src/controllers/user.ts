import { StatusCodes } from 'http-status-codes';
import { Controller, Middleware, Get, Post } from '@overnightjs/core';
import { Request, Response } from 'express';
import authentication from '../middleware/authentication';
import User from '../models/user';
import { Repository } from 'typeorm';
import { Logger } from '@overnightjs/logger';
import UserRole from '../enums/userRole';
import Component from '../models/component';
 
@Controller('user')
export class UserController {

    users: Repository<User>;
    components: Repository<Component>;

    constructor(users: Repository<User>, components: Repository<Component>) {
        this.users = users;
        this.components = components;
    }
 
    @Post()
    @Middleware([authentication])
    private async signup(request: Request, response: Response) {
        var body = request.body;
        Logger.Info(body.CPU);
        if(!body.name && !body.email) {
            return response.sendStatus(StatusCodes.BAD_REQUEST);
        }
        var user = new User();
        user.id = request.headers.user_id as string;
        user.name = request.body.name;
        user.email = request.body.email;
        user.roles = [UserRole.User];
        if(body.GPU != undefined && body.CPU != undefined) {
            Logger.Info("Components");
            try {
                user.CPU = await this.components.findOneOrFail(body.CPU);
                Logger.Info(user.CPU)
                user.GPU = await this.components.findOneOrFail(body.GPU);
                Logger.Info(user.GPU);
            } catch (e) {
                Logger.Err("Error when signing up");
                return response.sendStatus(StatusCodes.INTERNAL_SERVER_ERROR);
            }
        }
        this.users.save(user);
        Logger.Info("User signed up " + user.id);
        return response.status(StatusCodes.OK).json(user);
    }

    @Get()
    @Middleware([authentication])
    private getUser(request: Request, response: Response) {
        Logger.Info("Get user " + request.headers.user_id);
        this.users.findOne(request.headers.user_id as string, { relations: [ 'games' ] })
        .then(user => {
            if(user) {
                return response.status(StatusCodes.OK).json(user);
            } else {
                return response.status(StatusCodes.NOT_FOUND).json("Not Found");
            }
        })
        .catch(e => {
            Logger.Err(e);
            return response.sendStatus(StatusCodes.NOT_FOUND);
        });
    }

}