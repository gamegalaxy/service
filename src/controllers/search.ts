import { StatusCodes } from 'http-status-codes';
import { Controller, Get } from '@overnightjs/core';
import { query, Request, Response } from 'express';
import { Like, Repository } from 'typeorm';
import { Logger } from '@overnightjs/logger';
import Component from '../models/component';
import ComponentType from '../enums/componentType';
import Game from '../models/game';
 
@Controller('search')
export class SearchController {

    games: Repository<Game>;

    constructor(games: Repository<Game>) {
        this.games = games;
    }

    @Get('game')
    private autosuggest(request: Request, response: Response) {
        Logger.Info("Get autosuggest " + request.query.term);
        if(!request.query.term) {
            return response.sendStatus(StatusCodes.BAD_REQUEST);
        }
        this.games.find({  where: `"title" ILIKE '%${ request.query.term }%'` })
        .then(c => {
            if(c.length > 0) {
                return response.status(StatusCodes.OK).json(c);
            } else {
                return response.status(StatusCodes.NOT_FOUND).json("Not Found");
            }
        })
        .catch(e => {
            Logger.Err(e);
            return response.sendStatus(StatusCodes.NOT_FOUND);
        });
    }

}